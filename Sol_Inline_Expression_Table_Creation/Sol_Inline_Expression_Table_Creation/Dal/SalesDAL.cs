﻿using Sol_Inline_Expression_Table_Creation.Dal.ORD;
using Sol_Inline_Expression_Table_Creation.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Sol_Inline_Expression_Table_Creation.Dal
{
    public class SalesDAL
    {
        #region declaration

        private SalesDCDataContext _dc = null;

        #endregion

        #region  constructor

        public SalesDAL()
        {
            _dc = new SalesDCDataContext();
        }

        #endregion

        #region  public methods

        public IEnumerable<SalesEntity> GetSalesPersonData()
        {
            var getQuery =
                _dc
                ?.SalesPersons
                ?.AsEnumerable()
                ?.Select((leSalesPersonObj) => new SalesEntity()
                {
                    BusinessEntityId = leSalesPersonObj?.BusinessEntityID,
                    SalesLastYear = leSalesPersonObj?.SalesLastYear,
                    ModifiedDate = leSalesPersonObj?.ModifiedDate
                })
                ?.Take(5)
                ?.ToList();

            return getQuery;
        }

        #endregion
    }
}