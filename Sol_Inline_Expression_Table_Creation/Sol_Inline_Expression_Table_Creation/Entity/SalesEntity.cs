﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_Inline_Expression_Table_Creation.Entity
{
    public class SalesEntity
    {
        public int? BusinessEntityId { get;set;}

        public decimal? SalesLastYear { get; set; }

        public DateTime? ModifiedDate { get; set; }
    }
}