﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Sol_Inline_Expression_Table_Creation.Default" %>

<%@ Import Namespace="Sol_Inline_Expression_Table_Creation.Entity" %>
<%@ Import Namespace="Sol_Inline_Expression_Table_Creation.Dal" %>
<!DOCTYPE html>

<style type="text/css">
    table{
            width:50%;
            margin:auto;
             border-collapse:collapse;
        }

        th,td {
            border:1px;
            border-style:solid;
            border-color:gray;
            text-align:center;
            padding:20px;
            margin:10px;
        }

        tr:nth-child(even){
            background-color:white            
        }
</style>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>

            <tr>
            <th>BusinessEntityId</th>
             <th>SalesLastYear</th>
             <th>ModifiedDate</th>
            </tr>

           <%
                //get data from sales dal
                var getData = new SalesDAL().GetSalesPersonData();
                 %>

            <%
                foreach (var obj in getData)
                {
                 %>         

            <tr>
                <td>
                    <span>

                        <%--using code block expression--%>
                       <% Response.Write(Server.HtmlEncode(obj.BusinessEntityId.ToString())); %>

                    </span>
                </td>

                <td>
                    <span>
                        <% Response.Write(Server.HtmlEncode(obj.SalesLastYear.ToString())); %>
                    </span>

                </td>

                <td>
                    <span>
                        <% Response.Write(Server.HtmlEncode(obj.ModifiedDate.ToString())); %>
                    </span>

                </td>
            </tr>

            <%
                }
                 %>
    


        </table>
        
    </div>
    </form>
</body>
</html>
